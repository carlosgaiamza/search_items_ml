//
//  ProductSearchMLProductDetailPresenter.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 15/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductSearchMLProductDetailViewDelegate.h"
#import "ProductSearchMLProductDTO.h"

@interface ProductSearchMLProductDetailPresenter : NSObject
    
- (instancetype)initWithProduct:(ProductSearchMLProductDTO *)productDetail andViewDelegate:(id<ProductSearchMLProductDetailViewDelegate>)viewDelegate;
    
- (void)updateViewWithProductInfo;
    
@end
