//
//  ProductSearchMLSearchMainViewDelegate.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ProductSearchMLProductDetailViewDelegate
    
- (void)reloadDetail;

- (void)setTitle:(NSString *)title andPrice:(NSNumber *)price andThumbnail:(NSString *)thumbnail andCurrentyId:(NSString *)currentyId andSoldQuantity:(NSNumber *)soldQuantity andAvailableQuantity:(NSNumber *)availableQuantity andCondition:(NSString *)condition;
    
@end
