//
//  ProductSearchMLSearchService.h
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductSearchMLSearchProvider : NSObject

    - (void)searchProductWithQuery:(NSString *)query success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure;
    
@end
