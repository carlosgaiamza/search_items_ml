//
//  ProductSearchMLProductDTO.m
//  ProductSearchML
//
//  Created by Carlos Gaia on 14/07/2018.
//  Copyright © 2018 Carlos Gaia. All rights reserved.
//

#import "ProductSearchMLProductDTO.h"

@implementation ProductSearchMLProductDTO

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"title": @"title",
             @"price": @"price",
             @"thumbnail": @"thumbnail",
             @"currencyId": @"currency_id",
             @"availableQuantity": @"available_quantity",
             @"soldQuantity": @"sold_quantity",
             @"condition": @"condition"
             };
}
    
- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    return self;
}

@end
